/*
CREATE A ROUTE
"/greeting"
*/

const http=require('http')

let url = require("url")
//Creates a variable 'port' to store the port number
const port = 4000

//creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) => {
	//Accessing the 'greating' route returns a message of 'Hello World'
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('Hello Again')
//Accessing the 'homepage' route returns a message of "This is Homepage"

	}else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('Welcome Home')
	}else{
		response.writeHead(400, {'Content-Type':'text/plain'})
		response.end('Page not available')
	}
})

//uses the "server" and "port" variables created above
server.listen(port)

//when the server is running, console will print this message:
console.log('Server running at localhost: 4000');

