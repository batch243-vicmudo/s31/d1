//use the 'require' directive to load the HTTP module of node JS

//Modules - a software component or part of a program that contains one or more routines
//http module - lets Node.js transfer data using the HTTP (HyperText Transfer Protocol)
//HTTP is a protocol that allows the fetching of resources such as HTML documents
//Clients (browser) and servers (nodeJS/ExpressJS applications) communicate by exchanging individual messages

// REQUEST - messages sent by the client (usually in a Web browser)
// RESPONSES - messages sent by the servers as an answer to the client


let http = require("http")

// CREATE A SERVER
/*
	The hhtp module has a createServer() methods
		-that accepts a function as an argument and allows server creation
		-The arguments passed in the function are request & response objects (data type) that contain methods that allow us to receive requests from the client and send the response back
		-Using the module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client.

*/

//Defines the Port number that the server will be listening to
http.createServer(function(request, response){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end('Hello World')
}).listen(4000)

console.log('Server running at localhost: 4000');
//A port is virtual point where network connections start and end

//Send a response back to the client 
//Use the writeHead() method
//Set a status code for the response - a 200 - OK

//Inputting the command tells our device to run node js
//node index.js 

//Install nodemon via terminal
// npm install -g nodemon

//To check if nodemon is already installed via terminal
// nodemon -v

/*	IMPORTANT NOTE:
	Installing the package will allow the server to automatically restart when files have been changed or updated
	"-g" refers to a global installation where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any project.
*/